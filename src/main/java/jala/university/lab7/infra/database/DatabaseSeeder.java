package jala.university.lab7.infra.database;

import jala.university.lab7.infra.database.models.postgres.Product;
import jala.university.lab7.infra.database.models.postgres.User;
import jala.university.lab7.infra.database.models.UserRole;
import jala.university.lab7.infra.database.repositories.postgres.ProductRepositoryPostgres;
import jala.university.lab7.infra.database.repositories.postgres.UserRepositoryPostgres;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DatabaseSeeder implements CommandLineRunner {
    private final ProductRepositoryPostgres productRepositoryPostgres;
    private final UserRepositoryPostgres userRepositoryPostgres;

    @Override
    public void run(String... args) throws Exception {
        createAdmin();
        seedProducts();
    }

    private void createAdmin() {
        if(userRepositoryPostgres.count() == 0 || userRepositoryPostgres.findByUsername("admin") == null) {
            userRepositoryPostgres.save(new User("admin", "admin", UserRole.ADMIN));

            System.out.println("Admin created.");
        } else {
            System.out.println("Admin already exists.");
        }
    }

    private void seedProducts() {
        if (productRepositoryPostgres.count() == 0) {
            List<Product> products = Arrays.asList(
                    new Product("Laptop Gamer Acer", new BigDecimal("3500.89"), 248),
                    new Product("Smartphone Xiaomi Redmi Note 13", new BigDecimal("1290.90"), 196),
                    new Product("Smartphone Samsung S23 Ultra", new BigDecimal("5090.90"), 96),
                    new Product("Smartphone POCO X6 Pro", new BigDecimal("3290.90"), 16),
                    new Product("Laptop Vaio", new BigDecimal("4290.90"), 32),
                    new Product("Tablet Samsung Galaxy Tab A7", new BigDecimal("299.99"), 150),
                    new Product("Monitor LG UltraWide 29\"", new BigDecimal("249.99"), 50),
                    new Product("Mechanical Keyboard Redragon K556", new BigDecimal("79.99"), 300),
                    new Product("Headphone Bluetooth JBL T450BT", new BigDecimal("39.99"), 400)
            );

            productRepositoryPostgres.saveAll(products);

            System.out.println("Database seeded.");
        } else {
            System.out.println("Database have data already.");
        }
    }
}
