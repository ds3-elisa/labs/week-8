package jala.university.lab7.infra.database;

import jala.university.lab7.infra.database.models.mongo.ProductCollection;
import jala.university.lab7.infra.database.models.mongo.UserCollection;
import jala.university.lab7.infra.database.repositories.mongo.ProductRepositoryMongo;
import jala.university.lab7.infra.database.repositories.mongo.UserRepositoryMongo;
import jala.university.lab7.infra.database.repositories.postgres.ProductRepositoryPostgres;
import jala.university.lab7.infra.database.repositories.postgres.UserRepositoryPostgres;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DatabaseMigrationPostgresToMongo implements CommandLineRunner {
    private final UserRepositoryPostgres userRepositoryPostgres;
    private final UserRepositoryMongo userRepositoryMongo;
    private final ProductRepositoryPostgres productRepositoryPostgres;
    private final ProductRepositoryMongo productRepositoryMongo;

    @Override
    public void run(String... args) throws Exception {
        if(!(userRepositoryMongo.count() > 0)) migrateUsers();
        if(!(productRepositoryMongo.count() > 0)) migrateProducts();
    }

    private void migrateUsers() {
        var usersFromPostgres = userRepositoryPostgres.findAll();

        for(var user : usersFromPostgres) {
            var userCollection = new UserCollection(user.getUsername(), user.getPassword(), user.getRole());
            userRepositoryMongo.save(userCollection);
        }
    }

    private void migrateProducts() {
        var productsFromPostgres = productRepositoryPostgres.findAll();

        for(var product : productsFromPostgres) {
            var productCollection = new ProductCollection(product.getLabel(), product.getPrice(), product.getStock());
            productRepositoryMongo.save(productCollection);
        }
    }
}
