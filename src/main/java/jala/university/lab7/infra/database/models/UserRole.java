package jala.university.lab7.infra.database.models;

import lombok.Getter;

@Getter
public enum UserRole {
    ADMIN("admin"),
    USER("user");

    final String role;

    UserRole(String role) {
        this.role = role;
    }
}
