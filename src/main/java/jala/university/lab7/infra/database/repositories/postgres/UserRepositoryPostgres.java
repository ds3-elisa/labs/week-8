package jala.university.lab7.infra.database.repositories.postgres;

import jala.university.lab7.infra.database.models.postgres.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositoryPostgres extends JpaRepository<User, String> {
    UserDetails findByUsername(String username);
}
