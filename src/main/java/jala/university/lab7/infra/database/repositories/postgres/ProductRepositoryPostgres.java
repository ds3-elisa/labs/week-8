package jala.university.lab7.infra.database.repositories.postgres;

import jala.university.lab7.infra.database.models.postgres.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepositoryPostgres extends JpaRepository<Product, String> {}
