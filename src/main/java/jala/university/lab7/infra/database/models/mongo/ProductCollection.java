package jala.university.lab7.infra.database.models.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;

@Document(collection = "products")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class ProductCollection {
    @Id
    private String id;
    private String label;
    private BigDecimal price;
    private Integer stock;

    @CreatedDate
    private Instant createdAt;

    public ProductCollection(String label, BigDecimal price, Integer stock) {
        this.label = label;
        this.price = price;
        this.stock = stock;
    }
}
