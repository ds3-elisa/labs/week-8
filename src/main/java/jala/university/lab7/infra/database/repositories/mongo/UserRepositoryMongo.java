package jala.university.lab7.infra.database.repositories.mongo;

import jala.university.lab7.infra.database.models.mongo.UserCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositoryMongo extends MongoRepository<UserCollection, String> {
    UserDetails findByUsername(String username);
}
