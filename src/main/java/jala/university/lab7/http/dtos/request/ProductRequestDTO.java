package jala.university.lab7.http.dtos.request;

import java.math.BigDecimal;

public record ProductRequestDTO(String label, BigDecimal price, int stock) {
}
