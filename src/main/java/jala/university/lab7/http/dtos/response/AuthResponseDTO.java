package jala.university.lab7.http.dtos.response;

public record AuthResponseDTO(String token) {
}
