package jala.university.lab7.http.dtos.request;

import org.hibernate.validator.constraints.Length;

public record AuthRequestDTO(@Length(min = 3) String username, @Length(min = 3) String password) {
}
