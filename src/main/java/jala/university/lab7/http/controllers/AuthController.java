package jala.university.lab7.http.controllers;

import jala.university.lab7.http.dtos.response.AuthResponseDTO;
import jala.university.lab7.infra.database.repositories.postgres.UserRepositoryPostgres;
import jala.university.lab7.http.dtos.request.AuthRequestDTO;
import jala.university.lab7.http.dtos.request.RegisterRequestDTO;
import jala.university.lab7.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final UserRepositoryPostgres userRepositoryPostgres;
    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<AuthResponseDTO> auth(@RequestBody AuthRequestDTO body) {
        var token = authService.login(body.username(), body.password());
        return ResponseEntity.ok(new AuthResponseDTO(token));
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody RegisterRequestDTO body) {
        authService.register(body.username(), body.password(), body.role());
        return ResponseEntity.ok().build();
    }
}
