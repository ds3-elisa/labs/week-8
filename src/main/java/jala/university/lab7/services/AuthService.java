package jala.university.lab7.services;

import jakarta.validation.Valid;
import jala.university.lab7.infra.database.models.UserRole;
import jala.university.lab7.infra.database.models.mongo.UserCollection;
import jala.university.lab7.infra.database.models.postgres.User;
import jala.university.lab7.infra.database.repositories.mongo.UserRepositoryMongo;
import jala.university.lab7.infra.database.repositories.postgres.UserRepositoryPostgres;
import jala.university.lab7.http.dtos.request.AuthRequestDTO;
import jala.university.lab7.http.dtos.request.RegisterRequestDTO;
import jala.university.lab7.http.dtos.response.AuthResponseDTO;
import jala.university.lab7.infra.security.TokenService;
import jala.university.lab7.services.exceptions.UserExistsException;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {
    private final ApplicationContext context;
    private final UserRepositoryMongo userRepositoryMongo;
    private final TokenService tokenService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepositoryMongo.findByUsername(username);
    }

    public String login(String username, String password){
        AuthenticationManager authenticationManager = context.getBean(AuthenticationManager.class);

        var usernamePassword = new UsernamePasswordAuthenticationToken(username, password);
        var auth = authenticationManager.authenticate(usernamePassword);
        return tokenService.generateToken((UserCollection) auth.getPrincipal());
    }

    public void register (String username, String password, UserRole role) {
        if (this.userRepositoryMongo.findByUsername(username) != null ) {
            throw new UserExistsException();
        }

        String encryptedPassword = new BCryptPasswordEncoder().encode(password);
        var newUser = new UserCollection(username, encryptedPassword, role);
        this.userRepositoryMongo.save(newUser);
    }
}
