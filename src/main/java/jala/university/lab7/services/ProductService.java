package jala.university.lab7.services;

import jala.university.lab7.infra.database.models.mongo.ProductCollection;
import jala.university.lab7.infra.database.models.postgres.Product;
import jala.university.lab7.infra.database.repositories.mongo.ProductRepositoryMongo;
import jala.university.lab7.infra.database.repositories.postgres.ProductRepositoryPostgres;
import jala.university.lab7.services.exceptions.ProductNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepositoryMongo productRepositoryMongo;

    public List<ProductCollection> findAll() {
        return productRepositoryMongo.findAll();
    }

    public ProductCollection findById(String id) {
        var product = productRepositoryMongo.findById(id);
        if(product.isEmpty()) {
            throw new ProductNotFoundException();
        }

        return product.get();
    }

    public ProductCollection create(ProductCollection product) {
        return productRepositoryMongo.save(product);
    }

    public void delete(String id) {
        var product = productRepositoryMongo.findById(id);

        if(product.isEmpty()) {
            throw new ProductNotFoundException();
        }

        productRepositoryMongo.delete(product.get());
    }
}
