package jala.university.lab7.services.exceptions;

public class UserExistsException extends RuntimeException {
    public UserExistsException() {
        super("User already exists.");
    }

    public UserExistsException(String message) {
        super(message);
    }
}
