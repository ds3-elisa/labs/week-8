package jala.university.lab7.services.exceptions;

public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException() {
        super("Product Not Found.");
    }

    public ProductNotFoundException(String message) {
        super(message);
    }
}
